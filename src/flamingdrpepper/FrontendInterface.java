package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface FrontendInterface { 
    //***************************************
    // Chat text area
    //***************************************
    public void appendChatText(String text);
    public void scrollChatToBottom();
    public void repaintChat();
    //***************************************
    // Client list
    //***************************************
    public void setClientListData(String[] data);
    public void repaintClients();
    public String[] getClients(String delimiter);
}
