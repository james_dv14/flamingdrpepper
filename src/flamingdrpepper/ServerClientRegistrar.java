package flamingdrpepper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerClientRegistrar extends BackendBase {
    //***************************************
    // Fields
    //***************************************
    private final ServerBackend server;
    protected ServerFrontend frontend;
    private final HashMap<String, ServerClientHandler> clients;
    private final ArrayList<String[]> history;
    
    // Server modules
    private final ServerPacketAssembler spa;
    private final ServerPacketSender sps;
    private final ServerMessageSender sms;
    //***************************************
    // Constructor
    //***************************************
    public ServerClientRegistrar(ServerBackend server) {
        this.server = server;
        clients = server.getClients();
        history = server.getHistory();
        
        // Server modules
        spa = server.getPacketAssembler();
        sps = server.getPacketSender();
        sms = server.getMessageSender();
    }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setFrontend (ServerFrontend f) { frontend = f; }
    //***************************************
    // Client connect/disconnect
    //***************************************
    public void registerClient(Connection connection) {
        String name = "Client_" + Helpers.getTimeStamp("ddHHmmssSSS");
        System.out.println("S: " + name + " has connected.");
        
        ServerClientHandler client = new ServerClientHandler();
        String status = "online";
        client.setStatus("online");
        client.setSender(connection);
        ServerReceiver receiver = new ServerReceiver(server, connection);
        client.setReceiver(receiver);
        
        clients.put(name, client);	
        frontend.updateClients();
        
        initializeName(name);
        
        int errors = sendMessageHistory(name); 
        if (errors > 0) {
            sms.sendMessage(name, SERVER_MESSAGE, errors + " past message(s) were not recovered.");
        }
        
        addClient(name, status);
        
        errors = sendClientList(name);
        if (errors > 0) {
            sms.sendMessage(name, SERVER_MESSAGE, errors + " online clients' data was not detected.");
        }
        
        (new Thread(receiver)).start();
    }
    
    public void unregisterClient(String name) {
        removeClient(name);
        sps.sendPacket(name, spa.getQuitPacket());
        clients.get(name).getReceiver().teardown();
        clients.remove(name);
        frontend.updateClients();
    }
    //***************************************
    // Client initialization
    //***************************************
    public boolean initializeName(String name) {
        return clients.get(name).getSender().sendObject(spa.getSetNamePacket(SELF, name));
    }
    
    public int sendMessageHistory(String recipient) {
        int errors = 0;
        for (String[] entry : history) {
            if (!(sms.sendMessage(recipient, entry[0], entry[1]))) {
                errors++;
            }
        }
        return errors;
    }
    
    public int sendClientList(String recipient) {
        int errors = 0;
        for (String name : clients.keySet()) {
            String[] packet = spa.getAddClientPacket(name, clients.get(name).getStatus());
            if (!clients.get(recipient).getSender().sendObject(packet)) {
                errors++;
            }
        }
        return errors;
    }
    //***************************************
    // Client-related commands
    //***************************************
    public boolean setName(String oldname, String newname) {
        clients.put(newname, clients.get(oldname));
        clients.remove(oldname);	
        frontend.updateClients();
        
        if (!sps.sendPacketToAllClients(spa.getSetNamePacket(oldname, newname))) {
            return false;
        }
        else {
            String[] entry = {SERVER_MESSAGE, oldname + " has changed name to " + newname + "."};
            history.add(entry);
            return sms.sendMessageToAllClients(entry[0], entry[1]);
        }
    }
    
    public boolean setStatus(String name, String status) {
        clients.get(name).setStatus(status);
        frontend.updateClients();
        
        if (!sps.sendPacketToAllClients(spa.getSetStatusPacket(name, status))) {
            return false;
        }
        else {
            String[] entry = {SERVER_MESSAGE, name + " has changed status to " + status + "."};
            history.add(entry);
            return sms.sendMessageToAllClients(entry[0], entry[1]);
        }
    }
    
    public boolean addClient(String name, String status) {
        if (!sps.sendPacketToAllClients(spa.getAddClientPacket(name, status))) {
            return false;
        }
        else {
            String[] entry = {SERVER_MESSAGE, name + " has connected."};
            history.add(entry);
            
            HashSet<String> exceptions = new HashSet();
            exceptions.add(name);
            
            if (!sms.sendMessageToAllClientsExcept(exceptions, entry[0], entry[1])) {
                return false;
            }
            else {
                return sms.sendMessage(name, entry[0], "You have connected.");
            }
        }
    }
    
    public boolean removeClient(String name) {
        HashSet<String> exceptions = new HashSet();
        exceptions.add(name);
        if (!sps.sendPacketToAllClientsExcept(exceptions, spa.getRemoveClientPacket(name))) {
            return false;
        }
        else { 
            String[] entry = {SERVER_MESSAGE, name + " has disconnected."};
            history.add(entry);
            return sms.sendMessageToAllClientsExcept(exceptions, entry[0], entry[1]);
        }
    }
    
}
