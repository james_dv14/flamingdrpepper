package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Client {
    public static final String DEFAULT_IP = "127.0.0.1";
    public static final int DEFAULT_PORT = 8888;
    public static final String QUIT_MESSAGE = "QUIT";
    
    public static void main(String args[]) {
        System.out.println();
        String ip_t = Helpers.promptMessage("Enter server IP (" + DEFAULT_IP  + "): ");
        String ip = Helpers.validIPv4(ip_t) ? ip_t : DEFAULT_IP;
        String port_t = Helpers.promptMessage("Enter port (" + DEFAULT_PORT + "): ");
        int port = Helpers.validPort(port_t) ? Integer.parseInt(port_t) : DEFAULT_PORT;
        System.out.println();
        
        ClientBackend backend = new ClientBackend(ip, port);
        ClientFrontend frontend = new ClientFrontend(backend);
        backend.setFrontend(frontend);
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                backend.teardown();
            }
        });
        
        (new Thread(backend)).start();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frontend.setVisible(true);
            }
        });
    }
}
