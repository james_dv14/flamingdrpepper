package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerClientHandler {
    private String status;
    private Connection sender;
    private ServerReceiver receiver;
    //***************************************
    // Accessors - getter
    //***************************************
    public String getStatus() { return status; }
    public Connection getSender() { return sender; }
    public ServerReceiver getReceiver() { return receiver; }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setStatus(String status) { this.status = status; } 
    public void setSender(Connection sender) { this.sender = sender; } 
    public void setReceiver(ServerReceiver receiver) { this.receiver = receiver; }
}
