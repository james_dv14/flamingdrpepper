package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class BackendBase {
    //***************************************
    // Server Commands
    //***************************************
    public static final String SET_NAME = "changename";
    public static final String SET_STATUS = "changestatus";
    public static final String WHISPER = "whisper";
    public static final String QUIT = "quit";
    public static final String SERVER_QUIT = "kahit ano na ilagay mo sa mga susunod";
    public static final String ADD_CLIENT = "kahit udo";
    public static final String REMOVE_CLIENT = "kahit isplakitengtengks";
    public static final String APPEND_MESSAGE = "kahit khangkungkernitzz";
    //***************************************
    // Other Constants
    //***************************************
    public static final String SELF = "kahit sabihin mong pogi ka, basta dito ka tumigil";
    public static final String COMMAND_STRING = "/";
    public static final String SERVER_MESSAGE = "Server Message";
}
