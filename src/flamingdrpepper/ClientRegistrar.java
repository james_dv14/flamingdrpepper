package flamingdrpepper;

import java.util.HashMap;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientRegistrar extends BackendBase {
    //***************************************
    // Fields
    //***************************************
    private final ClientBackend client;
    protected HashMap<String, String> clients;
    protected ClientFrontend frontend;
    //***************************************
    // Constructor
    //***************************************
    public ClientRegistrar(ClientBackend client) {
        this.client = client;
        clients = client.getClients();
    }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setFrontend(ClientFrontend f) { frontend = f; }
    //***************************************
    // Client-related client actions
    //***************************************
    public void setName(String oldname, String newname) {
        if (oldname.equals(SELF) || oldname.equals(client.getName())) {
            client.setName(newname);
        }
        clients.put(newname, clients.get(oldname));
        clients.remove(oldname);
        frontend.updateClients();
    }
    
    public void setStatus(String name, String status) {
        clients.put(name, status);
        frontend.updateClients();
    }
    
    public void addClient(String name, String status) {
        clients.put(name, status);
        frontend.updateClients();
    }
    
    public void removeClient(String name) {
        clients.remove(name);
        frontend.updateClients();
    }

}
