package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerPacketAssembler extends BackendBase {
    
    public String[] getSendMessagePacket(String sender, String message) {
        String[] packet = new String[3];
        
        packet[0] = APPEND_MESSAGE;
        packet[1] = sender;
        packet[2] = message;
        
        return packet;
    }
    
    public String[] getAddClientPacket(String name, String status) {
        String[] packet = new String[3];
        
        packet[0] = ADD_CLIENT;
        packet[1] = name;
        packet[2] = status;
        
        return packet;
    }
    
    public String[] getRemoveClientPacket(String name) {
        String[] packet = new String[2];
        
        packet[0] = REMOVE_CLIENT;
        packet[1] = name;
        
        return packet;
    }
    
    public String[] getSetNamePacket(String oldname, String newname) {
        String[] packet = new String[3];
        
        packet[0] = SET_NAME;
        packet[1] = oldname;
        packet[2] = newname;
     
        return packet;
    }
    
    public String[] getSetStatusPacket(String name, String status) {
        String[] packet = new String[3];
        
        packet[0] = SET_STATUS;
        packet[1] = name;
        packet[2] = status;
     
        return packet;
    }
    
    public String[] getQuitPacket() {
        String[] packet = new String[1];
        
        packet[0] = QUIT;
     
        return packet;
    }
    
    public String[] getServerQuitPacket() {
        String[] packet = new String[1];
        
        packet[0] = SERVER_QUIT;
     
        return packet;
    }
}
