package flamingdrpepper;

import java.util.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Server {
    public static final int DEFAULT_PORT = 8888;
    static ArrayList<Connection> connections;
    
    public static void main(String args[]) {
        System.out.println();
        String port_t = Helpers.promptMessage("Enter port (" + DEFAULT_PORT + "): ");
        int port = Helpers.validPort(port_t) ? Integer.parseInt(port_t) : DEFAULT_PORT;
        System.out.println();
        
        ServerBackend backend = new ServerBackend(port);
        ServerFrontend frontend = new ServerFrontend(backend);
        backend.setFrontend(frontend);
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                backend.teardown();
            }
        });
        
        (new Thread(backend)).start();
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frontend.setVisible(true);
            }
        });
    }
}
