package flamingdrpepper;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerMessageSender {
    //***************************************
    // Fields
    //***************************************
    private final HashMap<String, ServerClientHandler> clients;
    
    // Server modules
    private final ServerPacketAssembler spa;
    private final ServerPacketSender sps;
    //***************************************
    // Constructor
    //***************************************
    public ServerMessageSender(ServerBackend server) {
        clients = server.getClients();
        
        // Server modules
        spa = server.getPacketAssembler();
        sps = server.getPacketSender();
    }
    //***************************************
    // Methods
    //***************************************
    public boolean sendMessage(String recipient, String sender, String message) {
        try {
            clients.get(recipient).getSender().sendObject(spa.getSendMessagePacket(sender, message));
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public boolean sendMessageToAllClients(String sender, String message) {
        return sps.sendPacketToAllClients(spa.getSendMessagePacket(sender, message));
    }
    
    public boolean sendMessageToAllClientsExcept(HashSet<String> exceptions, String sender, String message) {
        return sps.sendPacketToAllClientsExcept(exceptions, spa.getSendMessagePacket(sender, message));
    }

}
