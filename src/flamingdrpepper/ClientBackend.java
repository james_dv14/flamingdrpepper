package flamingdrpepper;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientBackend extends BackendBase implements Runnable {
    //***************************************
    // Fields
    //***************************************
    // Thread sentinel
    private volatile boolean good;
    
    // Client data
    protected HashMap<String, String> clients;
    protected String ip;
    protected int port;
    protected Socket socket;
    protected Connection connection;
    protected String name;
    protected ClientFrontend frontend;
    
    // Client modules
    private final ClientPacketAssembler cpa;
    private final ClientMessageReceiver cmr;
    private final ClientRegistrar cr;
    private final ClientPacketParser cpp;
    //***************************************
    // Constructor
    //***************************************
    public ClientBackend (String ip, int port) {
    	this.ip = ip;
        this.port = port;
        clients = new HashMap();
        good = true;
        
        // Client modules
        cpa = new ClientPacketAssembler();
        cmr = new ClientMessageReceiver();
        cr = new ClientRegistrar(this);
        cpp = new ClientPacketParser(this);
    }
    //***************************************
    // Accessors - getter
    //***************************************
    public String getName() { return name; }
    public ClientFrontend getFrontend() { return frontend; }
    public HashMap<String, String> getClients() { return clients; }
    
    // Client modules
    public ClientMessageReceiver getMessageReceiver() { return cmr; }
    public ClientRegistrar getRegistrar() { return cr; }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setName(String n) { name = n; }
    public void setFrontend(ClientFrontend f) { 
        frontend = f; 
        cmr.setFrontend(f);
        cr.setFrontend(f);
        cpp.setFrontend(f);
    }
    //***************************************
    // Run
    //***************************************
    @Override
    public void run() {
        try {		
            System.out.println("C: Connecting to server...");
            socket = new Socket(ip, port);
            System.out.println("C: Connected.");   
            connection = new Connection(socket);
        }
        catch (UnknownHostException e ) {
            Helpers.showErrorDialog(e.getMessage(), "Mali IP mo, dre.");
            frontend.dispose();
        }
        catch (IOException e ) {
            Helpers.showErrorDialog(e.getMessage(), "Malamang mali IP mo, dre.");
            frontend.dispose();
        }
        catch (Exception e) {
            frontend.dispose();
        }
        try {
            while (good) {
                if(!receiveObject()) {
                    throw new Exception();
                }
            }
        }
        catch (ClientPacketParser.UdoException ue) {
            System.out.println("C: Terminated successfully.");
        }
        catch (Exception e) {
            frontend.dispose();
        }
    }
    //***************************************
    // Teardown
    //***************************************
    public void teardown(){
        try {
            connection.sendObject(cpa.getQuitPacket(name));
        }
        catch (NullPointerException ex) {
        }
        good = false;
        try {
            socket.close();
        } 
        catch (NullPointerException | IOException ex) {
        }
    }
    //***************************************
    // Send/receive objects
    //***************************************
    public boolean sendObject(Object o) {
        try {
            return connection.sendObject(o);
        }
        catch (NullPointerException e) {
            String msg = "Message not sent. ";
            msg += "You may have connected to the wrong IP address. ";
            msg += "Please wait while the program verifies if this is the case.";
            Helpers.showErrorDialog(msg, "Sumabog ka.");
            return false;
        }
    }
    
    public boolean receiveObject() throws ClientPacketParser.UdoException { 
        String[] packet = (String[])connection.receiveObject();
        return cpp.parsePacket(packet);
    }
}