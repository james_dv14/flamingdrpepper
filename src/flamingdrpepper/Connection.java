package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */

import java.io.*;
import java.net.*;

public class Connection {
    //***************************************
    // Fields
    //***************************************
    protected Socket s; 
    protected ObjectOutputStream out;
    protected ObjectInputStream in;
    //***************************************
    // Constructor
    //***************************************
    public Connection(Socket s) {
        // Initialize socket
        this.s = s;
        // Initialize object output stream
        try {
            out = new ObjectOutputStream(s.getOutputStream());
        }
        catch (IOException e) {
            System.out.print("ERROR: Cannot extract output stream.");
            e.printStackTrace();
        }
        // Initialize object input stream
        try {
            in = new ObjectInputStream(s.getInputStream());
        }
        catch (IOException e) {
            System.out.print("ERROR: Cannot extract input stream.");
            e.printStackTrace();
        }
    }
    //***************************************
    // Accessors (object)
    //***************************************
    public boolean sendObject(Object o) {
        try {
            out.writeObject(o);
            return true;
        }	
        catch (Exception e) {
            return false;
        }	
    }

    public Object receiveObject() {
        try {
            return in.readObject();
        }
        catch (IOException | ClassNotFoundException e) {
            return null;
        }
    }
    //***************************************
    // Accessors (misc)
    //***************************************
    public InetAddress getIp() { return s.getInetAddress(); }
}
