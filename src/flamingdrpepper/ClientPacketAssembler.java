package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientPacketAssembler extends BackendBase {
    
    public String[] getQuitPacket(String name) {
        String[] packet = new String[2];
        
        packet[0] = name;
        packet[1] = COMMAND_STRING + QUIT;
     
        return packet;
    }

}
