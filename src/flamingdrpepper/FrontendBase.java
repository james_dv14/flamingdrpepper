package flamingdrpepper;

import java.awt.Font;
import javax.swing.JFrame;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class FrontendBase extends JFrame implements FrontendInterface {
    //***************************************
    // Constants
    //***************************************
    public static final Font FONT_INPUT = new Font("Arial", Font.PLAIN, 12);
    public static final Font FONT_CHAT = new Font("Arial", Font.PLAIN, 12);
    public static final Font FONT_CLIENT = new Font("Arial", Font.PLAIN, 12);
    //***************************************
    // Common methods
    //***************************************
    public void appendMessage(String sender, String msg) {
        appendChatText(sender);
        appendChatText(": ");
        appendChatText(msg);
        appendChatText("\n");
        scrollChatToBottom();
        repaintChat();
    }
    
    public void updateClients() {
        setClientListData(getClients(" - "));
        repaintClients();
    }
}
