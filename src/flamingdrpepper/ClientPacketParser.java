package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientPacketParser extends BackendBase {
    //***************************************
    // Fields
    //***************************************
    protected ClientFrontend frontend;
    
    // Client modules
    private final ClientMessageReceiver cmr;
    private final ClientRegistrar cr;
    //***************************************
    // Constructor
    //***************************************
    public ClientPacketParser(ClientBackend client) {
        cmr = client.getMessageReceiver();
        cr = client.getRegistrar();
    }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setFrontend(ClientFrontend f) { frontend = f; }
    //***************************************
    // Methods
    //***************************************
    public boolean parsePacket(String[] packet) throws UdoException {
        try {
            switch (packet[0]) {
                case APPEND_MESSAGE:
                    cmr.appendMessage(packet[1], packet[2]);
                    break;
                case SET_NAME:
                    cr.setName(packet[1], packet[2]);
                    break;
                case SET_STATUS:
                    cr.setStatus(packet[1], packet[2]);
                    break;
                case ADD_CLIENT:
                    cr.addClient(packet[1], packet[2]);
                    break;
                case REMOVE_CLIENT:
                    cr.removeClient(packet[1]);
                    break;
                case QUIT:
                    frontend.dispose();
                    break;
                case SERVER_QUIT:
                    Helpers.showErrorDialog("Lost connection to server.", "Sumabog!");
                    frontend.dispose();
                    break;
            }
            return true;
        }
        catch (NullPointerException ne) {
            throw new UdoException();
        }
        catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    //***************************************
    // Exceptions
    //***************************************
    public static class UdoException extends Exception {

        public UdoException() {
        }
    }
}
