package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerMessageRouter extends BackendBase {
    //***************************************
    // Fields
    //***************************************
    private final ServerMessageSender sms;
    //***************************************
    // Constructor
    //***************************************
    public ServerMessageRouter(ServerBackend server) {
        // Server modules
        sms = server.getMessageSender();
    }
    //***************************************
    // Message routing
    //***************************************
    public boolean whisper(String sender, String whisper) {
        try {
            String recipient = whisper.substring(0, whisper.indexOf(" "));
            String message = whisper.substring(whisper.indexOf(" ") + 1, whisper.length());
            sms.sendMessage(sender, getWhisperRecipient(recipient), message);
            return sms.sendMessage(recipient, getWhisperSender(sender), message);
        }
        catch (IndexOutOfBoundsException e) {
            return sms.sendMessage(sender, SERVER_MESSAGE, "No message found.");
        }
        catch (Exception e) {
            return false;
        }
    }
    //***************************************
    // Utility functions
    //***************************************
    public String getWhisperSender(String name) {
        return "[" + name + " whispers]";
    }
    
    public String getWhisperRecipient(String name) {
        return "[to " + name + "]";
    }
    
}
