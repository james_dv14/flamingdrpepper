package flamingdrpepper;

import java.awt.event.KeyEvent;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientMessageSender {
    //***************************************
    // Fields
    //***************************************
    private final ClientFrontend f;
    private final ClientBackend client;
    private final ClientMessageCache cmc;
    //***************************************
    // Constructor
    //***************************************
    public ClientMessageSender(ClientFrontend frontend) {
        f = frontend;
        client = f.getBackend();
        cmc = f.getMessageCache();
    }
    //***************************************
    // Send messages
    //***************************************
    public void sendMessage() {
        String[] packet = new String[2];
        packet[0] = client.name;
        packet[1] = Helpers.removeTrailingWhitespace(f.getInputText());
        if (!packet[1].isEmpty()) {
            cmc.push(packet[1]);
            client.sendObject(packet);
            f.setInputText("");
        }
    }
    //***************************************
    // Resolve keyboard actions
    //***************************************
    public void resolveEnterPress(KeyEvent evt) {
        if (Helpers.isModifierDown(evt) ^ f.getEnterToSend()) {
            f.scrollInputToBottom();
        }
    }
    
    public void resolveEnterRelease(KeyEvent evt) {
        if (Helpers.isModifierDown(evt) && f.getEnterToSend()) { 
            f.appendInputText(System.lineSeparator());
        }
        else if (Helpers.isModifierDown(evt) || f.getEnterToSend()) {
            sendMessage();
        }
    }

}
