package flamingdrpepper;

import java.util.HashMap;
import java.util.HashSet;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerPacketSender {
    //***************************************
    // Fields
    //***************************************
    private final HashMap<String, ServerClientHandler> clients;
    //***************************************
    // Constructor
    //***************************************
    public ServerPacketSender(ServerBackend server) {
        clients = server.getClients();
    }
    //***************************************
    // Methods
    //***************************************
    public boolean sendPacket(String name, String[] packet) {
        return clients.get(name).getSender().sendObject(packet);
    }
    
    public boolean sendPacketToAllClients(String[] packet) {
        for (ServerClientHandler client : clients.values()) {
            if (!client.getSender().sendObject(packet)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean sendPacketToAllClientsExcept(HashSet<String> exceptions, String[] packet) {
        for (String name : clients.keySet()) {
            if (!exceptions.contains(name)) {
                if (!clients.get(name).getSender().sendObject(packet)) {
                    return false;
                }
            }
        }
        return true;
    }
}
