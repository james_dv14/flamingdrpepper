package flamingdrpepper;

import java.util.ArrayList;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerPacketParser extends BackendBase {
    //***************************************
    // Fields
    //***************************************
    private final ArrayList<String[]> history;
    
    // Server modules
    private final ServerMessageSender sms;
    private final ServerMessageRouter smr;
    private final ServerClientRegistrar scr;
    //***************************************
    // Constructor
    //***************************************
    public ServerPacketParser(ServerBackend server) {
        history = server.getHistory();
        
        // Server modules
        sms = server.getMessageSender();
        smr = server.getMessageRouter();
        scr = server.getClientRegistrar();
    }
    //***************************************
    // Methods
    //***************************************
    public boolean parsePacket(String[] packet) {
        try {
            if (packet[1].indexOf(COMMAND_STRING) == 0) {
                return extractCommandFromPacket(packet);
            }
            else {
                history.add(packet);
                return sms.sendMessageToAllClients(packet[0], packet[1]);
            }
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public boolean extractCommandFromPacket(String[] packet){
        String substr = packet[1].substring(COMMAND_STRING.length(), packet[1].length());
        String command;
        String value;
        try {
            if (substr.contains(" ")) {
                command = substr.substring(0, substr.indexOf(" "));
                value = substr.substring(substr.indexOf(" ") + 1, substr.length());
                return parseCommand(packet[0], command, value);
            }
            else {
                return parseCommand(packet[0], substr);
            }
        }
        catch (InvalidCommandException e) {
            String msg = "Invalid command '";
            msg += COMMAND_STRING;
            msg += e.command;
            msg += "'.";
            sms.sendMessage(packet[0], SERVER_MESSAGE, msg);
            return true;
        }
        catch (Exception e) {
            Helpers.showErrorDialog(e.getMessage(), "Sumabog");
            return false;
        }
    }
    
    public boolean parseCommand(String name, String command) throws InvalidCommandException {
        try {
            switch (command) {
                case QUIT:
                    scr.unregisterClient(name);
                default:
                    throw new InvalidCommandException(command);
            }
        }
        catch (InvalidCommandException e) {
            throw new InvalidCommandException(e.command);
        }
        catch (Exception e) {
            return false;
        }
    }
    
    public boolean parseCommand(String name, String command, String value) throws InvalidCommandException {
        try {
            switch (command) {
                case SET_NAME:
                    return scr.setName(name, Helpers.getFirstWord(value));
                case SET_STATUS:
                    return scr.setStatus(name, value);
                case WHISPER:
                    return smr.whisper(name, value);
                default:
                    throw new InvalidCommandException(command);
            }
        }
        catch (InvalidCommandException e) {
            throw new InvalidCommandException(e.command);
        }
        catch (Exception e) {
            return false;
        }
    }
    //***************************************
    // Exceptions
    //***************************************
    private static class InvalidCommandException extends Exception {
        String command;
        
        public InvalidCommandException(String command) {
            this.command = command;
        }
    }
}
