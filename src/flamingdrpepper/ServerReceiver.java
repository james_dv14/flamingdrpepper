package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerReceiver implements Runnable {
    //***************************************
    // Fields
    //***************************************
    ServerBackend server;
    Connection connection;
    private volatile boolean good;
    //***************************************
    // Constructor
    //***************************************
    public ServerReceiver(ServerBackend s, Connection c){
        server = s;
        connection = c;
        good = true;
    }
    //***************************************
    // Run
    //***************************************
    public void run() {
        try {
            while(good) {
                Object o = connection.receiveObject();
                server.receiveObject(o);
            }
        }
        catch (Exception e) {
            Helpers.showErrorDialog(e.getMessage(), "Sumabog");
        }
    }
    //***************************************
    // Teardown
    //***************************************
    public void teardown() {
        good = false;
    }

}
