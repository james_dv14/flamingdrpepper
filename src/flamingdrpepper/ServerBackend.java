package flamingdrpepper;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerBackend extends BackendBase implements Runnable {
    //***************************************
    // Fields
    //***************************************
    // Thread sentinel
    private volatile boolean good;
    
    // Server data
    protected int port;
    protected ServerSocket ssocket;
    protected HashMap<String, ServerClientHandler> clients;
    protected ArrayList<String[]> history;
    protected ServerFrontend frontend;
    
    // Server modules
    private final ServerPacketAssembler spa;
    private final ServerPacketSender sps;
    private final ServerMessageSender sms;
    private final ServerMessageRouter smr;
    private final ServerClientRegistrar scr;
    private final ServerPacketParser spp;
    //***************************************
    // Constructor
    //***************************************
    public ServerBackend (int port) {
        good = true;
        this.port = port;
        history = new ArrayList();
        clients = new HashMap();
        
        // Server modules
        spa = new ServerPacketAssembler();
        sps = new ServerPacketSender(this);
        sms = new ServerMessageSender(this);
        smr = new ServerMessageRouter(this);
        scr = new ServerClientRegistrar(this);
        spp = new ServerPacketParser(this);
    }
    //***************************************
    // Accessors - getter
    //***************************************
    public HashMap<String, ServerClientHandler> getClients() { return clients; }
    public ArrayList<String[]> getHistory() { return history; }
    
    // Server modules
    public ServerPacketAssembler getPacketAssembler() { return spa; }
    public ServerPacketParser getPacketParser() { return spp; }
    public ServerPacketSender getPacketSender() { return sps; }
    public ServerMessageSender getMessageSender() { return sms; }
    public ServerMessageRouter getMessageRouter() { return smr; }
    public ServerClientRegistrar getClientRegistrar() { return scr; }
    //***************************************
    // Accessors - setter
    //***************************************
    public void setFrontend (ServerFrontend f) { 
        frontend = f; 
        scr.setFrontend(f);
    }
    //***************************************
    // Run
    //***************************************
    @Override
    public void run() {
        try {
            ssocket = new ServerSocket(port);
            System.out.println("S: Starting server...");
            System.out.println("S: Waiting for connections...");
            while(good) {
                waitForConnection(ssocket);
            }
        } 
        catch (Exception e) {
            Helpers.showErrorDialog(e.getMessage(), "Sumabog");
        }
    }
    //***************************************
    // Run sub-methods
    //***************************************
    public void waitForConnection(ServerSocket ssocket) {
        try {
            Socket s = ssocket.accept();
            Connection connection = new Connection(s);
            scr.registerClient(connection);	
        }
        catch (SocketException se) {
            System.out.println("S: Terminated successfully.");
        }
        catch (Exception e) {
            e.printStackTrace();
            Helpers.showErrorDialog(e.getMessage(), "Sumabog");
        }
    }
    //***************************************
    // Teardown
    //***************************************
    public void teardown() {
        for (String key : clients.keySet()) {
            clients.get(key).getSender().sendObject(spa.getServerQuitPacket());
            clients.get(key).getReceiver().teardown();
        }
        good = false;
        if (ssocket != null) {
            try {
                ssocket.close();
            } 
            catch (IOException ex) {
                Logger.getLogger(ServerBackend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    //***************************************
    // Receive objects
    //***************************************
    public boolean receiveObject(Object o) {
        String[] packet = (String[]) o;
        try {
            frontend.appendMessage(packet[0], packet[1]);
            return spp.parsePacket(packet);
        }
        catch (Exception e) {
            return false;
        }
    }
}