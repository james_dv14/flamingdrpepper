package flamingdrpepper;

import java.awt.event.KeyEvent;
import javax.swing.text.BadLocationException;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientMessageCache {
    //***************************************
    // Fields
    //***************************************
    private final Helpers.BoundedStack<String> cache;
    private final ClientFrontend f;
    private int cacheIndex;
    //***************************************
    // Constructor
    //***************************************
    public ClientMessageCache(ClientFrontend frontend) {
        f = frontend;
        cache = new Helpers.BoundedStack(ClientFrontend.CACHE_SIZE);
    }
    //***************************************
    // Accessors - push
    //***************************************
    public void push(String str) {
        cache.push(str);
        cacheIndex = cache.size() - 1;
    }
    //***************************************
    // Keyboard actions
    //***************************************
    public void resolveFirstArrowPress(KeyEvent evt) {
        try {
            if (cache.size() > 0) {
                if (!f.getInputText().isEmpty() && 
                        !f.getInputText().equals(cache.get(cache.size() - 1))) {
                    cache.push(Helpers.removeTrailingWhitespace(f.getInputText()));
                    cacheIndex = cache.size() - 2;
                }
                else {
                    cacheIndex = cache.size() - 1;
                }
                f.setInputText(cache.get(cacheIndex));
                f.setTraversingCache(true);
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            catchOutOfBoundsException();
        }
            
    }
    
    public void resolveSucceedingArrowPress(KeyEvent evt){
        try {
            if (cache.size() > 0) {
                if (evt.getKeyCode() == KeyEvent.VK_UP) {
                    cacheIndex = decrementCacheIndex();
                    f.setInputText(cache.get(cacheIndex));
                }
                else if (evt.getKeyCode() == KeyEvent.VK_DOWN) {
                    cacheIndex = incrementCacheIndex();
                    f.setInputText(cache.get(cacheIndex));
                }
                else {
                    f.setTraversingCache(false);
                    cacheIndex = cache.size() - 1;
                }
            }
        }
        catch (ArrayIndexOutOfBoundsException e) {
            catchOutOfBoundsException();
        }
    }
    
    public void catchOutOfBoundsException() {
        System.out.println("Mahina! Mahina mahina mahina!");
        System.out.println("Hindi naman ako magkacrash dahil sa ginagawa mo, pero...");
        System.out.println("Pwede bang bagalan mo yung pagpindot ng arrow keys sa susunod?");
    }
    //***************************************
    // Cache traversal
    //***************************************
    public int incrementCacheIndex() {
        return Helpers.getPositiveMod(cacheIndex + 1, cache.size());
    }
    
    public int decrementCacheIndex() {
        return Helpers.getPositiveMod(cacheIndex - 1, cache.size());
    }
    //***************************************
    // Predicate functions
    //***************************************
    public boolean onFirstLine(javax.swing.JTextArea ta) {
        try {
            return ta.getCaretPosition() <= ta.getLineEndOffset(0);
        } 
        catch (BadLocationException ex) {
            return false;
        }
    }
}
