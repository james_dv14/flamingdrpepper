package flamingdrpepper;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;
import java.util.Map;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Helpers {
    //***************************************
    // Prompts
    //***************************************
    
    /**
     * Returns console input the user provides in response to a given prompt.
     * @param prompt The prompt.
     * @return The console input as a string.
     */
    public static String promptMessage(String prompt) {
        String msg;
        String defaultPrompt = "Enter message: ";
        if (prompt == null || prompt.isEmpty()) {
            System.out.print(defaultPrompt);
        }
        else {
            System.out.print(prompt);
        }
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        try {
            msg = input.readLine();
        }
        catch (IOException e) {
            msg = "";
        }
        return msg;
    }
    
    //***************************************
    // Dialogs
    //***************************************
    
    /**
     * Shows an error message in a JOptionPane for errors.
     * @param message The error message to show.
     * @param title The title of the dialog box.
     */
    public static void showErrorDialog(String message, String title) {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.ERROR_MESSAGE);
    }
    
    //***************************************
    // String sanitation
    //***************************************
    
    public static String removeTrailingWhitespace(String str) {
        return str.replaceFirst("\\s+$", "");
    }
    
    public static String getFirstWord(String str) {
        return str.contains(" ") ? str.substring(0, str.indexOf(" ")) : str;
    }
    
    //***************************************
    // Math
    //***************************************
    
    /**
     * Gets the positive result of a modulus operation, even with a negative
     * dividend.
     * @param a The dividend.
     * @param b The divisor.
     * @return The positive result of the modulus operation.
     */
    public static int getPositiveMod (int a, int b) {
        return (a % b + b) % b;
    }
    
    //***************************************
    // Conversions
    //***************************************
    
    /**
     * Converts a HashMap of Strings into a String array. Each element in the 
     * array contains the key and the value of an element in the hash map,
     * optionally separated by a delimiter.
     * @credit StackOverflow user arvind
     * @source "Iterate through a HashMap [duplicate]". StackOverflow.
     * @url http://stackoverflow.com/questions/1066589/iterate-through-a-hashmap
     * @param hashmap A HashMap of Strings.
     * @param delimiter A string which delimits the key and value when they are
     * compressed into a single array element.
     * @return The HashMap as an array.
     */
    public static String[] stringHashMapToArray(HashMap<String, String> hashmap, String delimiter) {
        String[] ret_array = new String[hashmap.size()];
        Iterator iter = hashmap.entrySet().iterator();
        int i = 0;
        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            String key = (String)entry.getKey();
            String value = (String)entry.getValue();
            ret_array[i] = key + delimiter + value;
            i++;
        }
        return ret_array;
    }
    
    //***************************************
    // Date and Time
    //***************************************
    
    /**
     * Returns a timestamp of the current date/time in the specified format.
     * @param format A format for SimpleDateFormat.
     * @return A timestamp of the current date/time in the specified format.
     */
    public static String getTimeStamp(String format) {
        return new SimpleDateFormat(format).format(new Date());
    }
    
    //***************************************
    // Validation
    //***************************************
    
    /**
     * Checks if a given string is a valid IPv4 address
     * @credit StackOverflow user rouble
     * @source "Validating IPv4 string in Java". StackOverflow.
     * @url http://stackoverflow.com/questions/4581877/validating-ipv4-string-in-java
     * @param ip The candidate IP address, as a string.
     * @return True if the string is a valid IPv4 address, false otherwise.
     */
    public static boolean validIPv4 (String ip) {
        try {
            // Check if string is null or empty.
            if (ip == null || ip.isEmpty()) {
                return false;
            }
            // Check if string has 4 parts.
            String[] parts = ip.split("\\.", -1);
            if ( parts.length != 4 ) {
                return false;
            }
            // Check if each part is an integer from 0-255.
            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if (i < 0 || i > 255) {
                    return false;
                }
            }
            //
            return true;
        } 
        catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    /**
     * Checks if a given string is a valid port number (0-65535).
     * @param port The candidate port number, as a string.
     * @return  True if the string is a valid port number, false otherwise.
     */
    public static boolean validPort (String port) {
        try {
            // Check if string is null or empty.
            if (port == null || port.isEmpty()) {
                return false;
            }
            // Check if the string is an integer from 0-65535.
            int i = Integer.parseInt(port);
            return !(i < 0 || i > 65535);
        } 
        catch (NumberFormatException nfe) {
            return false;
        }
    }
    
    //***************************************
    // Other predicate functions
    //***************************************
    
    /**
     * Returns whether a modifier (Alt, Ctrl or Shift) is down during a key 
     * event.
     * @param evt The key event.
     * @return True if a modifier was down during the key event, false 
     * otherwise.
     */
    public static boolean isModifierDown (KeyEvent evt) {
        return evt.isAltDown() || evt.isControlDown() || evt.isShiftDown();
    }
    
    public static boolean isUpOrDownArrow (KeyEvent evt) {
        return (evt.getKeyCode() == KeyEvent.VK_UP) 
                || (evt.getKeyCode() == KeyEvent.VK_DOWN);
    }
    
    /**
     * Returns whether a JTextArea's caret is on its first line.
     * @param ta The JTextArea.
     * @return True if the caret is on the first line, false otherwise. 
     */
    public static boolean isOnFirstLine(JTextArea ta) {
        try {
            return ta.getCaretPosition() <= ta.getLineEndOffset(0);
        } 
        catch (BadLocationException ex) {
            return false;
        }
    }
    
    //***************************************
    // Custom data structures
    //***************************************
    public static class BoundedStack<T> extends Stack<T> {
        private final int size;

        /**
        * Creates a stack with a maximum size.
        * @credit StackOverflow user Mickelain
        * @source "Creating a fixed-size Stack". StackOverflow.
        * @url http://stackoverflow.com/questions/7727919/creating-a-fixed-size-stack
        * @param size The maximum size of the stack.
        */
        public BoundedStack(int size) {
            super();
            this.size = size;
        }

        /**
         * Pushes an element to the stack. If there is no room, the oldest 
         * element(s) are removed until there is room.
         * @param object The element to push.
         * @return The element.
         */
        @Override
        public Object push(Object object) {
            while (size() >= size) {
                remove(0);
            }
            return super.push((T) object);
        }
    }
}
