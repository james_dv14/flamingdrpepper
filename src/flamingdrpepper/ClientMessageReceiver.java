package flamingdrpepper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientMessageReceiver {
    //***************************************
    // Fields
    //***************************************
    protected ClientFrontend frontend;
    //***************************************
    // Accessors - setter
    //***************************************
    public void setFrontend(ClientFrontend f) { frontend = f; }
    //***************************************
    // Methods
    //***************************************
    public void appendMessage(String sender, String message) {
        frontend.appendMessage(sender, message);
    }
}
