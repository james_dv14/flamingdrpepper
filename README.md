# CS 145 MP1 (Conference Chat) #

## Usage ##

### Set up a server ###

```
$ java flamingdrpepper.Server
```

1. Enter the port number in the command prompt.

### Open a client ###

```
$ java flamingdrpepper.Client
```

1. Enter the server's IP address in the command prompt.
2. Enter the port number in the command prompt.

## Build Details ##

Developed on NetBeans IDE 8.0.2 using Oracle Java 1.8.0_40.

## Bonus Features ##

Following is a list of features not explicitly covered in the MP1 spec. They were generally added to bring the program's functionality and robustness closer to that of a real-world application than a school requirement.

* The Connection class developed in CS 145 lab was modified to send (Serializable) Objects rather than Strings. While it is conceivable that this particular MP could have been implemented without this tweak, it will probably come in handy later.

* The client frontend includes a "Press Enter to send" checkbox. By default, Enter adds a new line and Ctrl/Alt/Shift+Enter sends the message. Ticking this checkbox exchanges those roles.

* The client frontend allows the user to retrieve the last message they sent by pressing the Up arrow at the topmost line. Once they have done this, they can cycle through a fixed number of recent messages using the Up and Down arrows.

* Terminating the client by closing the frontend window or pressing Ctrl+C at the terminal executes the teardown routine normally exclusive to the "/quit" command.

* Terminating the server by closing the frontend window or pressing Ctrl+C at the terminal notifies all clients that the connection has been lost and initiates their teardown routines.

* Connecting the client to the local machine's IP address while a server is not running displays an error dialog and closes the client.

* Connecting the client to an arbitrary IP address initially displays the client window normally. However, if no server program is present on that address and the client attempts to send a message, an error dialog is displayed telling the user the connection may time out shortly.